package com.shlee.recommnet.test;

import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.impl.model.file.FileDataModel;
import org.apache.mahout.cf.taste.impl.neighborhood.ThresholdUserNeighborhood;
import org.apache.mahout.cf.taste.impl.recommender.GenericUserBasedRecommender;
import org.apache.mahout.cf.taste.impl.similarity.PearsonCorrelationSimilarity;
import org.apache.mahout.cf.taste.model.DataModel;
import org.apache.mahout.cf.taste.neighborhood.UserNeighborhood;
import org.apache.mahout.cf.taste.recommender.RecommendedItem;
import org.apache.mahout.cf.taste.recommender.UserBasedRecommender;
import org.apache.mahout.cf.taste.similarity.UserSimilarity;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by leesangho on 2016. 12. 19..
 */
public class RecommendTest {

    public static void main(String[] args) {

        String resourcePath = "/Users/leesangho/IdeaProjects/RecommendTest/src/main/resources";

        String dataPath = resourcePath + "/recommend.csv";
        String testPath = resourcePath + "/test.csv";
        int testId = 5;

        List<RecommendedItem> recommends = null;
        try {
            recommends = excuteTest(dataPath, testId, 0.1);
        } catch (IOException e) {
            e.printStackTrace();
            return;
        } catch (TasteException e) {
            e.printStackTrace();
            return;
        }

        testAccuracy(TestDataPaser.run(testPath), recommends);

    }

    private static List<RecommendedItem> excuteTest(String dataPath, int user_id, double threshold)
            throws  IOException, TasteException {

        DataModel model = new FileDataModel(new File(dataPath));
        UserSimilarity sim;
        sim = new PearsonCorrelationSimilarity(model);

        UserNeighborhood neighborhood = new ThresholdUserNeighborhood(threshold, sim, model);
        UserBasedRecommender recommender = new GenericUserBasedRecommender(model, neighborhood, sim);
        return recommender.recommend(user_id, 10);
    }


    private static void testAccuracy(List<Long> prefers, List<RecommendedItem> items) {

        if (items.isEmpty()) {
            System.out.println("items has no item");
            return;
        }

        for (RecommendedItem recommendation : items) {
            System.out.println(recommendation);
        }

        System.out.println(String.format("testing for %d test set --", prefers.size()));
        int hit = 0;
        int miss = 0;
        for (Long prefer : prefers) {

            if (isHit(items, prefer))
                hit += 1;
            else
                miss += 1;
        }

        System.out.println(String.format("[hit : %d] [miss : %d] [total : %d]", hit, miss, hit + miss));
    }
    
    private static boolean isHit(List<RecommendedItem> recommends, long test) {

        for (RecommendedItem recommend : recommends) {
            if (test == recommend.getItemID())
                return true;
        }
        return false;
    }

}
