package com.shlee.recommnet.test;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class TestDataPaser {

    public static List<Long> run(String path) {
        BufferedReader br = null;
        String line;

        List<Long> prefers = new ArrayList<Long>();

        // File Input 스트림 생성
        FileInputStream fis = null;

        // Input 스트림 생성
        InputStreamReader isr = null;

        // 버퍼로 읽어들일 임시 변수
        String temp = "";

        try {
            // 파일을 읽어들여 File Input 스트림 객체 생성
            fis = new FileInputStream(new File(path));

            // File Input 스트림 객체를 이용해 Input 스트림 객체를 생서하는데 인코딩을 UTF-8로 지정
            isr = new InputStreamReader(fis, "UTF-8");

            // Input 스트림 객체를 이용하여 버퍼를 생성
            br = new BufferedReader(isr);

            // 버퍼를 한줄한줄 읽어들여 내용 추출
            while( (temp = br.readLine()) != null) {
                if (!temp.equals(""))
                    prefers.add(Long.valueOf(temp));
//                System.out.println(temp);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return prefers;
    }
}
